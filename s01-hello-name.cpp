/*
 * The very first program you should write in any new programming language.
 */


#include <iostream>


auto main() -> int
{
    std::cout << "Hello, Janek!\n";
    return 0;
}
